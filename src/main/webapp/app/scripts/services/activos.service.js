(function () {
    'use strict';
    angular.module('activoFijoAngularApp.service').service('activosService', activosService);
    activosService.$inject = ['$http', '$q'];
    function activosService($http, $q) {
        var servicioActivo = this;
        servicioActivo.buscarTodosActivosFijos = getFindAllActivosFijos;
        servicioActivo.buscarEstadoAndTipo= getFindEstadoAndTipo;
        servicioActivo.buscarEstadoAndSerial= getFindEstadoAndSerial;
        servicioActivo.buscarEstadoAndFechaCompra= getFindEstadoAndFechaCompra;
        servicioActivo.guardarActivoFijo = saveActivoFijo;
        servicioActivo.actualizarActivoFijo = updateActivoFijo;
        servicioActivo.activoFijo = {};
        servicioActivo.activoFijoFilter = {};
        servicioActivo.activoFijoAuxiliar = {};
        //URL de consumos de services api rest angular
        var url = 'http://localhost:8080/apiActivoFijo/rest/api/activoFijo';
                
        function getFindAllActivosFijos() {
            var defered = $q.defer(); 
            var urlRequest = url;
            $http.get(urlRequest).then(function onSuccess(response) {
                // Handle success
                defered.resolve(response.data); 
              }, function onError(response) {
                // Handle error
                defered.reject(response);
              });
            return defered.promise;
        }
        
        function getFindEstadoAndTipo(estado,tipo) {
            var defered = $q.defer(); 
            var urlRequest = url+'/estadoAndTipo/'+estado+'/'+tipo;
            $http.get(urlRequest).then(function onSuccess(response) {
                // Handle success
                defered.resolve(response.data); 
              }, function onError(response) {
                // Handle error
                defered.reject(response);
              });
            return defered.promise;
        }
        
        function getFindEstadoAndSerial(estado,serial) {
            var defered = $q.defer(); 
            var urlRequest = url+'/estadoAndSerial/'+estado+'/'+serial;
            $http.get(urlRequest).then(function onSuccess(response) {
                // Handle success
                defered.resolve(response.data); 
              }, function onError(response) {
                // Handle error
                defered.reject(response);
              });
            return defered.promise;
        }
        
        function getFindEstadoAndFechaCompra(estado,fechaCompra) {
            var defered = $q.defer(); 
            var urlRequest = url+'/estadoAndFechaCompra/'+estado+'/'+fechaCompra;
            $http.get(urlRequest).then(function onSuccess(response) {
                // Handle success
                defered.resolve(response.data); 
              }, function onError(response) {
                // Handle error
                defered.reject(response);
              });
            return defered.promise;
        }

        function saveActivoFijo(rs) {
            var defered = $q.defer();
            var urlRequest = url;
            $http.post(urlRequest, rs).then(function onSuccess(response) {
                // Handle success
                defered.resolve(response.data);
              }).catch(function onError(response) {
                // Handle error
                defered.reject(response);
              });                        
            return defered.promise;
        }

        function updateActivoFijo(id,rs) {
            var defered = $q.defer();
            var urlRequest = url+'/'+id;
            $http.put(urlRequest, rs).then(function onSuccess(response) {
                // Handle success
                defered.resolve(response.data);
            }).catch(function onError(response) {
                // Handle error
                defered.reject(response);
            });                        
            return defered.promise;
        }
        
    }
})();

