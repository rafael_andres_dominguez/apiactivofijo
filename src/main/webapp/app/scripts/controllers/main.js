'use strict';

/**
 * @ngdoc function
 * @name activoFijoAngularApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the activoFijoAngularApp
 */
angular.module('activoFijoAngularApp').controller('MainCtrl', ['$scope','activosService','$filter', function ($scope,activosService,$filter) {
    var activoFijoMain = this;
    activoFijoMain.activoFijoFilter = activosService.activoFijo;
    activoFijoMain.cabeceras = ['#','Nombre', 'Tipo', 'Serial', 'Valor Compra', 'Fecha Compra','Fecha de Baja','Estado', 'Opciones'];
    activoFijoMain.listActivosFijos = [];
    activoFijoMain.dateAsString = [];
    activoFijoMain.tipos = [{id:'Inmueble',nombreTipo:"Inmueble"},{id:'Maquinaria',nombreTipo:"Maquinaria"},{id:'Oficina',nombreTipo:"Oficina"}];
    activoFijoMain.estados = [
        {id:'TODOS',nombreEstado:"TODOS"},
        {id:'ACTIVO',nombreEstado:"Activo"},
        {id:'DISPONIBLE',nombreEstado:"Disponible"},
        {id:'DADODEBAJA',nombreEstado:"Dado de Baja"},
        {id:'ENREPARACION',nombreEstado:"En Reparación"},
        {id:'ASIGNADO',nombreEstado:"Asignado"}
    ];

    function onConsultarActivos() {
        activosService.buscarTodosActivosFijos().then(function (data) {
            activoFijoMain.listActivosFijos=data.responseList;
        });
    }
    
    activoFijoMain.onCambiarSelectTipo = function () {
        activoFijoMain.listActivosFijos = [];
        activosService.buscarEstadoAndTipo(activoFijoMain.activoFijoFilter.estado,activoFijoMain.activoFijoFilter.tipo).then(function (data) {
            console.log(activoFijoMain.activoFijoFilter.tipo);
            console.log(activoFijoMain.activoFijoFilter.estado);
            console.log(data);
            activoFijoMain.listActivosFijos = data.responseList;
        }).catch(function (e) {
            return;
        });
    };
    
    activoFijoMain.onCambiarSelectEstado = function () {
        if(activoFijoMain.activoFijoFilter.estado === 'TODOS'){
            activoFijoMain.listActivosFijos = [];
            onConsultarActivos();
        }
    };
    
    activoFijoMain.onCambiarSerial = function () {
        activoFijoMain.listActivosFijos = [];
        activosService.buscarEstadoAndSerial(activoFijoMain.activoFijoFilter.estado,activoFijoMain.activoFijoFilter.serial).then(function (data) {
            console.log(activoFijoMain.activoFijoFilter.serial);
            console.log(activoFijoMain.activoFijoFilter.estado);
            console.log(data);
            activoFijoMain.listActivosFijos = data.responseList;
        }).catch(function (e) {
            return;
        });
    };
    
    activoFijoMain.onCambiarFechaCompra = function () {
        activoFijoMain.listActivosFijos = [];
//        activoFijoMain.dateAsString = $filter('date')(activoFijoMain.activoFijoFilter.fechaCompra, "yyyy-MM-dd");
        console.log($filter('date')(activoFijoMain.activoFijoFilter.fechaCompra, "dd/MM/yyyy"));
        activosService.buscarEstadoAndFechaCompra(activoFijoMain.activoFijoFilter.estado,
                                                  "'"+$filter('date')(activoFijoMain.activoFijoFilter.fechaCompra,"dd/MM/yyyy")+"'").then(function (data) {
            activoFijoMain.listActivosFijos = data.responseList;
        }).catch(function (e) {
            return;
        });
    };
    
    onConsultarActivos();
    
    function onLimpiar() {
        activoFijoMain.activoFijoObject.id = null;
        activoFijoMain.activoFijoObject.nombre = '';
        activoFijoMain.activoFijoObject.descripcion = '';
        activoFijoMain.activoFijoObject.tipo = null;
        activoFijoMain.activoFijoObject.serial = null;
        activoFijoMain.activoFijoObject.numeroInternoInventario = null;
        activoFijoMain.activoFijoObject.peso = null;
        activoFijoMain.activoFijoObject.alto = null;
        activoFijoMain.activoFijoObject.ancho = '';
        activoFijoMain.activoFijoObject.largo = '';
        activoFijoMain.activoFijoObject.valorCompra = null;
        activoFijoMain.activoFijoObject.fechaCompra = null;
        activoFijoMain.activoFijoObject.fechaBaja = null;
        activoFijoMain.activoFijoObject.estado = null;
    }
    
    activoFijoMain.onClickToUpdate = function (item) {
        
    };
  }]);