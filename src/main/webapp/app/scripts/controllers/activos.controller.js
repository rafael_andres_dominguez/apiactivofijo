'use strict';
angular.module('activoFijoAngularApp').controller('ActivosCtrl', ['$scope','activosService','$location',
    function ($scope, activosService, $location) {
        var activoFijoControl = this;
        activoFijoControl.activoFijoObject = activosService.activoFijo;
        activoFijoControl.activoFijoFilter = activosService.activoFijoFilter;
        activoFijoControl.activoFijoAuxiliar = activosService.activoFijoAuxiliar;
        activoFijoControl.cabeceras = ['#','Nombre', 'Tipo', 'Serial', 'Valor Compra', 'Fecha Compra','Fecha de Baja','Estado', 'Opciones'];
        activoFijoControl.dateAsString = [];        
        activoFijoControl.listaActivosFijos = [];
        activoFijoControl.tipos = [{id:'Inmueble',nombreTipo:"Inmueble"},{id:'Maquinaria',nombreTipo:"Maquinaria"},{id:'Oficina',nombreTipo:"Oficina"}];
        activoFijoControl.estadosCreate = [{id:'ACTIVO',nombreEstado:"Activo"},
            {id:'DISPONIBLE',nombreEstado:"Disponible"},{id:'DADODEBAJA',nombreEstado:"Dado de Baja"},
            {id:'ENREPARACION',nombreEstado:"En Reparación"},{id:'ASIGNADO',nombreEstado:"Asignado"}
        ];
        activoFijoControl.estados = [{id:'TODOS',nombreEstado:"TODOS"},
            {id:'ACTIVO',nombreEstado:"Activo"},{id:'DISPONIBLE',nombreEstado:"Disponible"},
            {id:'DADODEBAJA',nombreEstado:"Dado de Baja"},{id:'ENREPARACION',nombreEstado:"En Reparación"},
            {id:'ASIGNADO',nombreEstado:"Asignado"}
        ];
        
        activoFijoControl.onSubmitForm = function () {
            if (activoFijoControl.activoFijoObject.id === null || typeof activoFijoControl.activoFijoObject.id === 'undefined') {
                activoFijoControl.onNewActivoFijo();
            } else {
                activoFijoControl.onUpdateActivoFijo();
            }
        };

        activoFijoControl.onLimpiar = function () {
            activoFijoControl.activoFijoObject.id = null;
            activoFijoControl.activoFijoObject.nombre = null;
            activoFijoControl.activoFijoObject.descripcion = null;
            activoFijoControl.activoFijoObject.tipo = null;
            activoFijoControl.activoFijoObject.serial = null;
            activoFijoControl.activoFijoObject.numeroInternoInventario = null;
            activoFijoControl.activoFijoObject.peso = null;
            activoFijoControl.activoFijoObject.alto = null;
            activoFijoControl.activoFijoObject.ancho = null;
            activoFijoControl.activoFijoObject.largo = null;
            activoFijoControl.activoFijoObject.valorCompra = null;
            activoFijoControl.activoFijoObject.fechaCompra = null;
            activoFijoControl.activoFijoObject.fechaBaja = null;
            activoFijoControl.activoFijoObject.estado = null;
        };

        activoFijoControl.onUpdateActivoFijo = function () {
            var updateActivoFijo ={
                id: activoFijoControl.activoFijoObject.id,
                serial: activoFijoControl.activoFijoObject.serial,
                fechaBaja: activoFijoControl.activoFijoObject.fechaBaja,
                estado:activoFijoControl.activoFijoObject.estado
            };
            activosService.actualizarActivoFijo(activoFijoControl.activoFijoObject.id,updateActivoFijo).then(function (data) {
                switch (data.tipo) {
                    case 200:
                        alert('Activo Fijo Actualizado Correctamente.');
                        activoFijoControl.onLimpiar();
                        break;
                    case 400:
                        alert('No se logro realizar la operación.'+data.message);
                        break;
                    default:
                        alert('Error al procesar la operación.'+data.message);
                        break;
                }
            });
        };
        
        activoFijoControl.onNewActivoFijo = function () {
            var newActivoFijo = {
                id: null,
                nombre: activoFijoControl.activoFijoObject.nombre,
                descripcion: activoFijoControl.activoFijoObject.descripcion,
                tipo: activoFijoControl.activoFijoObject.tipo,
                serial: activoFijoControl.activoFijoObject.serial,
                numeroInternoInventario: activoFijoControl.activoFijoObject.numeroInternoInventario,
                peso: activoFijoControl.activoFijoObject.peso,
                alto: activoFijoControl.activoFijoObject.alto,
                ancho: activoFijoControl.activoFijoObject.ancho,
                largo: activoFijoControl.activoFijoObject.largo,
                valorCompra: activoFijoControl.activoFijoObject.valorCompra,
                fechaCompra: activoFijoControl.activoFijoObject.fechaCompra,
                fechaBaja: activoFijoControl.activoFijoObject.fechaBaja,
                estado: activoFijoControl.activoFijoObject.estado                    
            };
            activosService.guardarActivoFijo(newActivoFijo).then(function (data) {
                switch (data.tipo) {
                    case 200:
                        alert('Activo Fijo Registrado Correctamente.');
                        activoFijoControl.onLimpiar();
                        break;
                    case 400:
                        alert('No se logro realizar la operación.'+data.message);
                        break;
                    default:
                        alert('Error al procesar la operación.'+data.message);
                        break;
                }
            });
        };

        activoFijoControl.onClickToEditar = function (item) {
            activoFijoControl.onLimpiar();
            activoFijoControl.activoFijoAuxiliar.disableEdit = true;

            activoFijoControl.activoFijoObject.id = item.id;
            activoFijoControl.activoFijoObject.nombre= item.nombre;
            activoFijoControl.activoFijoObject.descripcion= item.descripcion;
            activoFijoControl.activoFijoObject.tipo= item.tipo;
            activoFijoControl.activoFijoObject.serial= item.serial;
            activoFijoControl.activoFijoObject.numeroInternoInventario= item.numeroInternoInventario;
            activoFijoControl.activoFijoObject.peso= item.peso;
            activoFijoControl.activoFijoObject.alto= item.alto;
            activoFijoControl.activoFijoObject.ancho= item.ancho;
            activoFijoControl.activoFijoObject.largo= item.largo;
            activoFijoControl.activoFijoObject.valorCompra= item.valorCompra;
            activoFijoControl.activoFijoObject.fechaCompra= item.fechaCompra;
            activoFijoControl.activoFijoObject.fechaBaja= item.fechaBaja;
            activoFijoControl.activoFijoObject.estado= item.estado;

            $location.path('/createActivoFijo');
        };

        /*Functions consultas*/
        function onConsultarActivos() {
            activosService.buscarTodosActivosFijos().then(function (data) {
                activoFijoControl.listActivosFijos=[];
                angular.forEach(data.responseList, function (value, key) {
                    convertToString(value);
                });
            });
        }
    
        activoFijoControl.onCambiarSelectTipo = function () {
            activoFijoControl.listActivosFijos = [];
            activosService.buscarEstadoAndTipo(activoFijoControl.activoFijoFilter.estado,activoFijoControl.activoFijoFilter.tipo).then(function (data) {
                //activoFijoControl.listActivosFijos = data.responseList;
                angular.forEach(data.responseList, function (value, key) {
                    convertToString(value);
                });
            }).catch(function (e) {
                return;
            });
        };

        activoFijoControl.onCambiarSelectEstado = function () {
            if(activoFijoControl.activoFijoFilter.estado === 'TODOS'){
                activoFijoControl.listActivosFijos = [];
                onConsultarActivos();
            }
        };

        activoFijoControl.onCambiarSerial = function () {
            if(activoFijoControl.activoFijoFilter.serial !== null
                   && typeof activoFijoControl.activoFijoFilter.serial !== 'undefined'
                   && activoFijoControl.activoFijoFilter.serial !== ''){
                activoFijoControl.listActivosFijos = [];
                activosService.buscarEstadoAndSerial(activoFijoControl.activoFijoFilter.estado,activoFijoControl.activoFijoFilter.serial).then(function (data) {
                    angular.forEach(data.responseList, function (value, key) {
                        convertToString(value);
                    });
                }).catch(function (e) {
                    return;
                });
            }
        };

        activoFijoControl.onCambiarFechaCompra = function () {
            activoFijoControl.listActivosFijos = [];
//            console.log($filter('date')(activoFijoControl.activoFijoFilter.fechaCompra, "dd/MM/yyyy"));
            console.log(activoFijoControl.activoFijoFilter.fechaCompra);
            activosService.buscarEstadoAndFechaCompra(activoFijoControl.activoFijoFilter.estado,
                                        activoFijoControl.activoFijoFilter.fechaCompra).then(function (data) {
//                activoFijoControl.listActivosFijos = data.responseList;
                angular.forEach(data.responseList, function (value, key) {
                    convertToString(value);
                });
            }).catch(function (e) {
                console.log('error');
                return;
            });
        };
        
        function convertToString(value) {
            var dateCompra= value.fechaCompra !== null ? new Date(value.fechaCompra) : null;
            var dateBaja = value.fechaBaja !== null ? new Date(value.fechaBaja) : null;
            var actFijo = {
                id: value.id,
                nombre: value.nombre,
                descripcion: value.descripcion,
                tipo: value.tipo,
                serial: value.serial,
                numeroInternoInventario:value.numeroInternoInventario ,
                peso : value.peso,
                alto : value.alto,
                ancho: value.ancho,
                largo: value.largo,
                valorCompra: value.valorCompra,
                fechaCompra: value.fechaCompra!== null ? new Date(value.fechaCompra):null,
                fechaBaja: value.fechaBaja !== null ? new Date(value.fechaBaja): null,
                fechaCompraString: dateCompra !== null ? dateCompra.toLocaleString(): null,//value.fechaCompra,
                fechaBajaString: dateBaja !== null ? dateBaja.toLocaleString() : null,
                estado: value.estado
            };
            activoFijoControl.listActivosFijos.push(actFijo);            
        }
        
        onConsultarActivos();
    }]);


