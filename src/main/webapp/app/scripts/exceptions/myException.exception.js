(function() {
  'use strict';

  angular.module('activoFijoAngularApp.exceptions')
    .factory('$exceptionHandler',myException);

    function myException(){
      var exc = function (excep, cause){
        console.log(excep);
      };
      return exc;
    }
})();
