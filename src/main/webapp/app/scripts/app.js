'use strict';

/**
 * @ngdoc overview
 * @name activoFijoAngularApp
 * @description
 * # activoFijoAngularApp
 *
 * Main module of the application.
 */
angular.module('activoFijoAngularApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'activoFijoAngularApp.service',
    'activoFijoAngularApp.exceptions',
    'xeditable'
  ])
  .config(function ($routeProvider) {
    $routeProvider
        .when('/', {
          templateUrl: 'app/views/main.html',
          controller: 'MainCtrl'
        })
        .when('/about', {
          templateUrl: 'app/views/about.html',
          controller: 'AboutCtrl',
          controllerAs: 'about'
        })
        .when('/createActivoFijo', {
          templateUrl: 'app/views/createActivoFijo.html',
          controller: 'ActivosCtrl'
        })
      .otherwise({
        redirectTo: '/'
      });
  });
