/**
 * 
 */
var controllersModule = angular.module('controllers', []);
 
controllersModule.controller('SampleController', function($scope) {
    console.log('Carga de Ativos Fijo!');
});
 
controllersModule.controller('ajaxController', function($scope, $http) {
    $http.get('/activoFijo/rest/activoFijo/').success(function(data) {
        console.log(data);
    });
});