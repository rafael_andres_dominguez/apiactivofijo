/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.activofijo.spring.data.repository;

import com.activofijo.spring.domain.object.ActivoFijo;
import java.util.Date;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;
 
/**
 * Repository interface to manage ActivoFijo instances.
 *
 * @author DESARROLLADOR12
 */
@RepositoryRestResource(collectionResourceRel="activoFijo",path="activoFijo")
public interface ActivoFijoRepository extends MongoRepository<ActivoFijo, ObjectId>{
    /**
    * Consulta de Activos Fijos por estado
    * 
    * @param estado
    * @return
    */
    List<ActivoFijo> findByEstado(@Param("estado") String estado);
    
    /**
    * Consulta de Activos Fijos por estado y tipo
    * 
    * @param estado
    * @param tipo
    * @return
    */
    List<ActivoFijo> findByEstadoAndTipo(@Param("estado") String estado,@Param("tipo") String tipo);
    
    /**
    * Consulta de Activos Fijos por estado y fecha de compra
    * 
    * @param estado
    * @param fechaCompra
    * @return
    */
    List<ActivoFijo> findByEstadoAndFechaCompra(String estado,Date fechaCompra);
    
    /**
    * Consulta de Activos Fijos por estado y serial
    * 
    * @param estado
    * @param serial
    * @return
    */
    List<ActivoFijo> findByEstadoAndSerial(@Param("estado") String estado,@Param("serial") String serial);
}
