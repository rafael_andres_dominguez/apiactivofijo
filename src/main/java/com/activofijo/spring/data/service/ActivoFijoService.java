/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.activofijo.spring.data.service;

import com.activofijo.spring.domain.object.ActivoFijo;
import com.activofijo.spring.util.ResponseObject;
import java.util.Date;

/**
 * Service interface to manage ActivoFijo instances.
 *
 * @author DESARROLLADOR12
 */
public interface ActivoFijoService {
    /**
    * Servicio que retorna todos los Activos Fijos.
    *  
    * @return
    */
    ResponseObject findAll();
    
    /**
    * Servicio de busqueda de Activos Fijos por id
    * 
    * @param id
    * @return
    */
    ActivoFijo findOne(String id);
    
    /**
    * Servicio de busqueda de Activos Fijos por estado y tipo
    * 
    * @param estado
    * @param tipo
    * @return
    */
    ResponseObject findEstadoAndTipo(String estado,String tipo);
    
    /**
    * Servicio de busqueda de Activos Fijos por estado y fecha de compra
    * 
    * @param estado
    * @param fechaCompra
    * @return
    */
    ResponseObject findEstadoAndFechaCompra(String estado,Date fechaCompra);
    
    /**
    * Servicio de busqueda de Activos Fijos por estado y serial
    * 
    * @param estado
    * @param serial
    * @return
    */
    ResponseObject findEstadoAndSerial(String estado,String serial);
    
    /**
    * Servicio de registro de Activos Fijos
    * 
    * @param activoFijo
    * @return
    */
    ResponseObject save(ActivoFijo activoFijo);
}
