/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.activofijo.spring.data.service.impl;

import com.activofijo.spring.data.repository.ActivoFijoRepository;
import com.activofijo.spring.data.service.ActivoFijoService;
import com.activofijo.spring.domain.object.ActivoFijo;
import com.activofijo.spring.util.ResponseObject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author DESARROLLADOR12
 */
@Service
public class ActivoFijoServiceImpl implements ActivoFijoService{
    @Autowired
    ActivoFijoRepository activoFijoRepository;
    
    @Override
    public ResponseObject findAll() {
        ResponseObject responseObject = new ResponseObject();
        
        try {
            responseObject.setResponseList(activoFijoRepository.findAll());
            if(!responseObject.getResponseList().isEmpty()){
                responseObject.setMessage("Proceso Exitoso.");
                responseObject.setTipo(200);
            }else{
                responseObject.setMessage("Busqueda sin resultados.");
                responseObject.setTipo(404);
            }
        } catch (Exception ex) {
        }
        return responseObject;
    }

    @Override
    public ResponseObject findEstadoAndTipo(String estado, String tipo) {
        ResponseObject responseObject = new ResponseObject();
        try {
            responseObject.setResponseList(activoFijoRepository.findByEstadoAndTipo(estado, tipo));
            if(!responseObject.getResponseList().isEmpty()){
                responseObject.setMessage("Proceso Exitoso.");
                responseObject.setTipo(200);
            }else{
                responseObject.setMessage("Busqueda sin resultados.");
                responseObject.setTipo(404);
            }
        } catch (Exception ex) {
        }
        return responseObject;
    }

    @Override
    public ResponseObject findEstadoAndFechaCompra(String estado, Date fechaCompra) {
        ResponseObject responseObject = new ResponseObject();

        try {
            responseObject.setResponseList(activoFijoRepository.findByEstadoAndFechaCompra(estado, fechaCompra));
            if(!responseObject.getResponseList().isEmpty()){
                responseObject.setMessage("Proceso Exitoso.");
                responseObject.setTipo(200);
            }else{
                responseObject.setMessage("Busqueda sin resultados.");
                responseObject.setTipo(404);
            }
        } catch (Exception ex) {
        }
        return responseObject;
    }

    @Override
    public ResponseObject findEstadoAndSerial(String estado, String serial) {
        ResponseObject responseObject = new ResponseObject();

        try {
            responseObject.setResponseList(activoFijoRepository.findByEstadoAndSerial(estado, serial));
            if(!responseObject.getResponseList().isEmpty()){
                responseObject.setMessage("Proceso Exitoso.");
                responseObject.setTipo(200);
            }else{
                responseObject.setMessage("Busqueda sin resultados.");
                responseObject.setTipo(404);
            }
        } catch (Exception ex) {
        }
        return responseObject;
    }

    @Override
    public ActivoFijo findOne(String id) {
        try {
            return activoFijoRepository.findOne(new ObjectId(id));
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public ResponseObject save(ActivoFijo activoFijo) {
        ResponseObject responseObject = new ResponseObject();
        try {
            if(activoFijo.getFechaCompra() != null && activoFijo.getFechaBaja() != null ){
                if(activoFijo.getFechaCompra().compareTo(activoFijo.getFechaBaja()) < 0 ){
                    responseObject.setMessage("La fecha de baja no puede ser superior a la fecha de compra.");
                    responseObject.setTipo(400);
                }else{
                    List<ActivoFijo> listActivo = new ArrayList<>();
                    listActivo.add(activoFijoRepository.save(activoFijo));
                    responseObject.setResponseList(listActivo);
                    responseObject.setMessage("Proceso Exitoso");
                    responseObject.setTipo(200);
                }
            }else{
                List<ActivoFijo> listActivo = new ArrayList<>();
                listActivo.add(activoFijoRepository.save(activoFijo));
                responseObject.setResponseList(listActivo);
                responseObject.setMessage("Proceso Exitoso");
                responseObject.setTipo(200);
            }
        } catch (Exception ex) {
        }
        return responseObject;
    }
    
}
