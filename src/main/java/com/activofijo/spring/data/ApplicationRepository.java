/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.activofijo.spring.data;

import com.activofijo.spring.data.repository.ActivoFijoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;
/**
 *
 * @author DESARROLLADOR12
 */
@Service
public class ApplicationRepository {

    @Autowired
    private MongoTemplate mongoTemplate;
    @Autowired
    private ActivoFijoRepository activoFijoRepository;
 
    /**
     * @return the mongoTemplate
     */
    public MongoTemplate getMongoTemplate() {
        return mongoTemplate;
    }
 
    /**
     * @return the activoFijoRepository
     */
    public ActivoFijoRepository getActivoFijoRepository() {
        return activoFijoRepository;
    }

}
