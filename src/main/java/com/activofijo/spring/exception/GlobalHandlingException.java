/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.activofijo.spring.exception;

import com.activofijo.spring.util.ResponseObject;
import java.util.logging.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 *
 * @author DESARROLLADOR12
 */
@ControllerAdvice
public class GlobalHandlingException extends Exception{
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ResponseObject> exceptionHandler(Exception ex) {
        ResponseObject error = new ResponseObject();
        error.setTipo(HttpStatus.INTERNAL_SERVER_ERROR.value());
        error.setMessage("!!! Exception 1 ...."+ ex.getMessage());
        Logger.getLogger(ex.getMessage());
        return new ResponseEntity<>(error,HttpStatus.OK);
    }

    @ExceptionHandler(DataAccessException.class)
    public ResponseEntity<ResponseObject>  handleDataAccessException(DataAccessException ex) {
        ResponseObject error = new ResponseObject();
        error.setTipo(HttpStatus.INTERNAL_SERVER_ERROR.value());
        error.setMessage("!!! DataAccessException 2...."+ ex.getMessage());
        Logger.getLogger(ex.getMessage());
        return new ResponseEntity<>(error,HttpStatus.CONFLICT);
    }

    @ExceptionHandler({NullPointerException.class})
    public ResponseEntity<ResponseObject>  nullPointerError(Exception ex) {
        ResponseObject error = new ResponseObject();
        error.setTipo(HttpStatus.INTERNAL_SERVER_ERROR.value());
        error.setMessage("!!! NULL POINTER..."+ex.getMessage());
        Logger.getLogger(ex.getMessage());
        return new ResponseEntity<>(error,HttpStatus.OK);
    }

}
