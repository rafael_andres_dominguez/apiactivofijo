/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.activofijo.spring.domain.comun;

/**
 *
 * @author DESARROLLADOR12
 */
public enum EstadoActual {
    
    ACTIVO("ACTIVO"), 
    DADO_DE_BAJA("DADO DE BAJA"),
    EN_REPARACION("EN REPARACION"),
    DISPONIBLE("DISPONIBLE"),
    ASIGNADO("ASIGNADO");

    private String valor;

    private EstadoActual(String valor) {
        this.valor = valor;
    }

    private EstadoActual() {
    }
    
    public String getValor() {
        return valor;
    }
}
