/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.activofijo.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.activofijo.spring.data.service.ActivoFijoService;
import com.activofijo.spring.domain.object.ActivoFijo;
import com.activofijo.spring.util.ResponseObject;
import java.util.Calendar;
import java.util.Date;
import javax.ws.rs.Produces;
import org.bson.types.ObjectId;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author DESARROLLADOR12
 */
@RestController
@Produces({"application/xml", "application/json"})
@RequestMapping(value = "/api/activoFijo")
public class ApplicationController {
    @Autowired
    ActivoFijoService activoFijoService;
 
    @RequestMapping(method=RequestMethod.GET)
    public ResponseObject getAll() {
        return activoFijoService.findAll();
    }
    
    @RequestMapping(method=RequestMethod.GET,value="estadoAndTipo/{estado}/{tipo}")
    public ResponseObject getAllEstadoAndTipo(@PathVariable("estado") String estado,@PathVariable("tipo") String tipo) {
        return activoFijoService.findEstadoAndTipo(estado, tipo);
    }
    
    @RequestMapping(method=RequestMethod.GET,value="estadoAndFechaCompra/{estado}/{fechaCompra}")
    public ResponseObject getAllEstadoAndFechaCompra(@PathVariable("estado") String estado,
            //@RequestParam("from") @DateTimeFormat(iso=ISO.DATE) Date fromDate
            @PathVariable("fechaCompra") @DateTimeFormat(pattern="yyyy-MM-dd") Date fechaCompra) {
        Calendar fecha=Calendar.getInstance();
        fecha.setTime(fechaCompra);
        fecha.add(Calendar.DAY_OF_YEAR, 1);
        return activoFijoService.findEstadoAndFechaCompra(estado, fecha.getTime());
    }
    
    @RequestMapping(method=RequestMethod.GET,value="estadoAndSerial/{estado}/{serial}")
    public ResponseObject getAllEstadoAndSerial(@PathVariable("estado") String estado,@PathVariable("serial") String serial) {
        return activoFijoService.findEstadoAndSerial(estado, serial);
    }
    
    @RequestMapping(method=RequestMethod.PUT, value="{id}")
    public ResponseObject update(@PathVariable("id") String id, @RequestBody ActivoFijo activoNew) {
        ActivoFijo activoFijoUpdate = activoFijoService.findOne(id);
        if(activoFijoUpdate!=null){
            activoFijoUpdate.setSerial(activoNew.getSerial());
            activoFijoUpdate.setFechaBaja(activoNew.getFechaBaja());
            activoFijoUpdate.setEstado(activoNew.getEstado());
        }
        return activoFijoService.save(activoFijoUpdate);
    }
    
    @RequestMapping(method=RequestMethod.POST)
    public ResponseObject save(@RequestBody ActivoFijo activoFijo) {
        return activoFijoService.save(activoFijo);
    }
}
