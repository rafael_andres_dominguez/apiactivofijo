/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.activofijo.spring.util;

import com.fasterxml.jackson.databind.JsonNode;
import java.util.List;

/**
 *
 * @author DESARROLLADOR12
 */
public class ResponseObject {
    private String message;
    private Integer tipo;
    private List responseList;
    private Boolean configure;

    public ResponseObject(String message, Integer tipo) {
        this.message = message;
        this.tipo = tipo;
    }

    public ResponseObject(String message, Integer tipo, List responseList, JsonNode objectResponse) {
        this.message = message;
        this.tipo = tipo;
        this.responseList = responseList;
    }    

    public ResponseObject() {        
    }
    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }
 
    /**
     * @param message
     * the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }
    
        public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public List getResponseList() {
        return responseList;
    }

    public void setResponseList(List responseList) {
        this.responseList = responseList;
    }

    public Boolean getConfigure() {
        return configure;
    }

    public void setConfigure(Boolean configure) {
        this.configure = configure;
    }

}
